package Ejercicio1;


import java.awt.event.*;
import javax.swing.*;

public class AplicacionGrafica extends JFrame{

	// Panel de la aplicacion
	private JPanel contentPane;
	
	public AplicacionGrafica() {
		
		// CONTENEDOR
		
		// Titulo de la ventana
		setTitle("Saludador");
		// Tama�o ventana (x, y, longitud, altura)
		setBounds(600, 300, 500, 300);
		// Cerrar la app al cerrar la ventana
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		

		// PANEL
		
		// Crear el panel
		contentPane = new JPanel();
		// Indicar el dise�o del panel
		contentPane.setLayout(null);
		// Asignar el panel a la ventana
		setContentPane(contentPane);
		
		
		// COMPONENTES
		
		// Crear componente JLabel
		JLabel etiqueta = new JLabel("Escribe un nombre para saludar");
		// Colocar el componente (x, y, longitud, altura)
		etiqueta.setBounds(150,75,200,10);
		// A�adir el componente al panel
		contentPane.add(etiqueta);
		
		
		// Crear componente JTextField
		JTextField textField = new JTextField();
		// Colocar el componente (x, y, longitud, altura)
		textField.setBounds(155,125,175,20);
		// A�adir el componente al panel
		contentPane.add(textField);
		
		
		// Crear componente JButton
		JButton btnSaludar = new JButton("Saludar");
		// Colocar el componente (x, y, longitud, altura)
		btnSaludar.setBounds(195,175,100,20);
		// A�adir el componente al panel
		contentPane.add(btnSaludar);
		
		
		// Evento al pulsar el boton
		btnSaludar.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Hola " + textField.getText());
			}
		});
		

		// Hacer la ventana visible
		setVisible(true);
		
	}
	
}
