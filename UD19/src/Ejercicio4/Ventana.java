package Ejercicio4;
/*
 * @author David
 * 
 */

import java.awt.*;
import java.awt.event.*;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.*;

public class Ventana extends JFrame{
	
	private JPanel contentPane; //Panel de la aplicaci�n
	
	public Ventana() {
		setTitle("ejercicio 4 - Calculadora"); 		//Titulo de la ventana
		setBounds(400,200,450,220); 				//Cordenadas x,y,longitud,altura
		setDefaultCloseOperation(EXIT_ON_CLOSE); 	//Accion por defecto al cerrar la ventana
		
		//Creamos el panel, indicamos su dise�o y lo asignamos a la ventana
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane); 
		
		//Creamos un componente y lo a�adimos a la ventana
		JLabel etiqueta = new JLabel("Introduce los operandos");
		etiqueta.setBounds(60,20,150,20);
		contentPane.add(etiqueta);
		
		//Creamos un componente y lo a�adimos a la ventana
		JLabel operando = new JLabel("+");
		operando.setBounds(180,52,10,20);
		contentPane.add(operando);
		
		//Creamos un campo de texto y lo a�adimos a la ventana
		JTextField tF1 = new JTextField();
		tF1.setBounds(60,42,100,20);
		contentPane.add(tF1);
		
		//Creamos un campo de texto y lo a�adimos a la ventana
		JTextField tF2 = new JTextField();
		tF2.setBounds(60,64,100,20);
		contentPane.add(tF2);
		
		//Creamos un campo de texto y lo a�adimos a la ventana
		JTextField tfRes = new JTextField();
		tfRes.setBounds(60,86,200,20);
		tfRes.disable();
		contentPane.add(tfRes);
		
		
		//Creamos un boton y lo a�adimos a la ventana
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.setBounds(60,130,89,20);
		contentPane.add(btnCalcular);
		
		//Creamos un boton y lo a�adimos a la ventana
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(275,130,89,20);
		contentPane.add(btnSalir);
		//Creamos un boton y lo a�adimos a la ventana
		JButton btnAbout = new JButton("?");
		btnAbout.setBounds(364,130,20,20);
		contentPane.add(btnAbout);
		
		//Creamos un boton y lo a�adimos a la ventana
		JButton btnSuma = new JButton("+");
		btnSuma.setBounds(275,20,89,20);
		contentPane.add(btnSuma);
		
		//Creamos un boton y lo a�adimos a la ventana
		JButton btnResta = new JButton("-");
		btnResta.setBounds(275,42,89,20);
		contentPane.add(btnResta);
		
		//Creamos un boton y lo a�adimos a la ventana
		JButton btnDivision = new JButton("/");
		btnDivision.setBounds(275,64,89,20);
		contentPane.add(btnDivision);
		
		//Creamos un boton y lo a�adimos a la ventana
		JButton btnMulti = new JButton("x");
		btnMulti.setBounds(275,86,89,20);
		contentPane.add(btnMulti);

		setVisible(true); //Hace visible la ventana
		
		
		
		//Acciones de los botones +,-,/ y *
		btnSuma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operando.setText("+");
			}
		});
		btnResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operando.setText("-");
			}
		});
		btnDivision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operando.setText("/");
			}
		});
		btnMulti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operando.setText("*");
			}
		});
		
		//Accion del boton Calcular
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String sop1 = tF1.getText();
					String sop2 = tF2.getText();
					String op = operando.getText();
					double op1 = Double.parseDouble(sop1);
					double op2 = Double.parseDouble(sop2);
					double op3 = 0;
					switch(op){
					case "+":
						op3 = op1 + op2;
						tfRes.setText(" "+op3);
						break;
					case "-":
						op3 = op1 - op2;
						tfRes.setText(" "+op3);
						break;
					case "/":
						op3 = op1 / op2;
						tfRes.setText(" "+op3);
						break;
					case "*":
						op3 = op1 * op2;
						tfRes.setText(" "+op3);
						break;
					
					}
		        } catch (Exception ex) {
		            System.out.println("Error: " + ex);
		        }
				
			}
		});
		
		//Accion del boton about
		btnAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "@author David y Xavi. @Version alpha 0.1");
			}
		});
		
		//Accion del boton salir
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComponent comp = (JComponent) e.getSource();
				Window win = SwingUtilities.getWindowAncestor(comp);
				win.dispose();
			}	
		});
		
	}
	
	

}
