package Ejercicio2;
/*
 * @author David
 * 
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Ventana extends JFrame{
	
	private JPanel contentPane; //Panel de la aplicaci�n
	
	public Ventana() {
		setTitle("ejercicio 2"); 					//Titulo de la ventana
		setBounds(400,200,650,300); 				//Cordenadas x,y,longitud,altura
		setDefaultCloseOperation(EXIT_ON_CLOSE); 	//Accion por defecto al cerrar la ventana
		
		//Creamos el panel, indicamos su dise�o y lo asignamos a la ventana
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane); 
		
		//Creamos un componente y lo a�adimos a la ventana
		JLabel etiqueta = new JLabel("Escribe el nombre de una pelicula");
		etiqueta.setBounds(60,20,200,20);
		contentPane.add(etiqueta);
		
		//Creamos un campo de texto y lo a�adimos a la ventana
		JTextField tF = new JTextField();
		tF.setBounds(60,60,200,20);
		contentPane.add(tF);
		
		//Creamos un boton y lo a�adimos a la ventana
		JButton btnA�adir = new JButton("A�adir");
		btnA�adir.setBounds(60,130,89,20);
		contentPane.add(btnA�adir);
		
		//Creamos un componente y lo a�adimos a la ventana
		JLabel etiqueta2 = new JLabel("Peliculas");
		etiqueta2.setBounds(290,20,200,20);
		contentPane.add(etiqueta2);
		
		//Creamos un ComboBox y lo a�adimos a la ventana
		JComboBox comboBox = new JComboBox<>();
		comboBox.setBounds(290,60,320,20);
		contentPane.add(comboBox);
		
		setVisible(true); //Hace visible la ventana
		
		
		//Funcionalidad de la aplicaci�n
		btnA�adir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String entrada = tF.getText();
				if (entrada.equals("")) {
					JOptionPane.showMessageDialog(null, "Has de escribir texto!");
				}else {
					comboBox.addItem(entrada);
				}
			}
		});
		
	}
	
	

}
