package Ejercicio3;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AplicacionGrafica extends JFrame{

	// Panel de la aplicacion
	private JPanel contentPane;
	
	public AplicacionGrafica() {
		
		// CONTENEDOR
		
		// Titulo de la ventana
		setTitle("Encuesta");
		// Tama�o ventana (x, y, longitud, altura)
		setBounds(600, 300, 425, 325);
		// Cerrar la app al cerrar la ventana
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
		// PANEL
		
		// Crear el panel
		contentPane = new JPanel();
		// Indicar el dise�o del panel
		contentPane.setLayout(null);
		// Asignar el panel a la ventana
		setContentPane(contentPane);
		
		
		// COMPONENTES
		
		// Crear JLabel selecionarSistema
		JLabel selecionarSistema = new JLabel("Elije un sistema operativo:");
		selecionarSistema.setBounds(25,25,200,10);
		contentPane.add(selecionarSistema);
		
		// Crear JRadioButton rbtnOpcionSistema1
		JRadioButton rbtnOpcionSistema1 = new JRadioButton ("Windows", false);
		rbtnOpcionSistema1.setActionCommand("Windows");
		rbtnOpcionSistema1.setBounds(25,50,200,10);
		contentPane.add(rbtnOpcionSistema1);
		
		// Crear JRadioButton rbtnOpcionSistema2
		JRadioButton rbtnOpcionSistema2 = new JRadioButton ("Linux", false);
		rbtnOpcionSistema2.setActionCommand("Linux");
		rbtnOpcionSistema2.setBounds(25,75,200,10);
		contentPane.add(rbtnOpcionSistema2);
		
		// Crear JRadioButton rbtnOpcionSistema3
		JRadioButton rbtnOpcionSistema3 = new JRadioButton ("Mac", false);
		rbtnOpcionSistema3.setActionCommand("Mac");
		rbtnOpcionSistema3.setBounds(25,100,200,10);
		contentPane.add(rbtnOpcionSistema3);
		
		// Crear ButtonGroup
		ButtonGroup bgroup = new ButtonGroup();
		bgroup.add(rbtnOpcionSistema1);
		bgroup.add(rbtnOpcionSistema2);
		bgroup.add(rbtnOpcionSistema3);
		
		
		// Crear JLabel
		JLabel selecionarEspecialidad = new JLabel("Elije tu especialidad:");
		selecionarEspecialidad.setBounds(250,25,200,10);
		contentPane.add(selecionarEspecialidad);
		
		// Crear JCheckBox
		JCheckBox chckbxEspecialidad1 = new JCheckBox ("Programaci�n", false);
		chckbxEspecialidad1.setBounds(250,50,200,10);
		contentPane.add(chckbxEspecialidad1);
		
		// Crear JCheckBox
		JCheckBox chckbxEspecialidad2 = new JCheckBox ("Dise�o gr�fico", false);
		chckbxEspecialidad2.setBounds(250,75,200,10);
		contentPane.add(chckbxEspecialidad2);
		
		// Crear JCheckBox
		JCheckBox chckbxEspecialidad3 = new JCheckBox ("Administraci�n", false);
		chckbxEspecialidad3.setBounds(250,100,200,10);
		contentPane.add(chckbxEspecialidad3);
		
		
		// Crear JLabel
		JLabel horas = new JLabel("Horas dedicadas en el ordenador:");
		horas.setBounds(25,135,200,10);
		contentPane.add(horas);
		
		// Crear JSlider
		JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 10, 0);
		slider.setBounds(25,155,350,50);
		slider.setMajorTickSpacing(10);
		slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		contentPane.add(slider);
		
		
		// Crear JButton
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBounds(25,225,75,30);
		contentPane.add(btnEnviar);
		
		
		// Evento al pulsar el boton enviar para mostrar los datos selecionados
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				
				String especialidad = "";
				String sistema = "";
				
				// Comprovar si esta selecionado
				if(chckbxEspecialidad1.isSelected()) {
					especialidad = especialidad.concat(chckbxEspecialidad1.getText());
				}
				
				// Comprovar si esta selecionado
				if(chckbxEspecialidad2.isSelected()) {
					especialidad = especialidad.concat(", " + chckbxEspecialidad2.getText());
				}
				
				// Comprovar si esta selecionado
				if(chckbxEspecialidad3.isSelected()) {
					especialidad = especialidad.concat(", " + chckbxEspecialidad3.getText());
				}
				
				// Comprovar si esta selecionado
				if (bgroup.getSelection() != null) {
					sistema = bgroup.getSelection().getActionCommand();
				}
				
				// Mostrar mensaje con los datos
				JOptionPane.showMessageDialog(null, "Datos enviados ( Sistema: " + sistema + " | Especialidad: " + especialidad + " | Horas: " + slider.getValue() + " ) ");
			}
		});
		
		
		// Hacer la ventana visible
		setVisible(true);

	}
	
}
