![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)

# UD19 - Swing/AWTP

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| Xavier Bonet Daga | Master | Project Manager | 01/02/2021 |   |   |  |
| David Bonet Daga | Master | Team Member | 01/02/2021 |   |   |  |

#### 2. Description
```
Ejercicios Swing/AWTP
```

#### 3. Link a un demo con el proyecto desplegado: https://gitlab.com/xavi_bonet/ud19.git

```
UD19 - Swing/AWTP / https://gitlab.com/xavi_bonet/ud19.git
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
Java - jdk-8
IDE - Eclipse Enterprise
```
###### Command line 
```

```

#### 5. Screenshot imagen que indique cómo debe verse el proyecto.
